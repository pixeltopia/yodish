#!/usr/bin/env ruby

require 'pp'

module Yodish

  class Token

    attr_reader :type, :value

    def initialize(type, value)
      @type, @value = type, value
    end

    class << self
      [:b, :n, :s, :v, :r, :o].each do |name|
        define_method name do |value|
          Token.new(name, name == :v ? value.split('.').map(&:to_i) : value)
        end
      end
    end

  end

  class Evaluator

    attr_reader :halts

    def initialize(table = {}, &block)
      @table = block_given? ? block.call : table
    end

    def eval(line, *options)
      reset()
      tokens = tokenize(line)
      pp line, tokens if options.include?(:verbose)
      tokens.each do |token|
        break if @halts
        case token.type
        when :b, :v, :n, :s, :v, :r
          push(token)
        when :o
          exec(token.value)
        end
      end
      peek()
    end

    def reset
      @stack = []
      @halts = false
    end

    def halt
      @stack = [Token.b(false)]
      @halts = true
    end

    def peek
      @stack.last.value
    end

    def exec(value)
      case value
      when '='       then exec_eql()
      when '<'       then exec_lt()
      when '<='      then exec_lte()
      when '>'       then exec_gt()
      when '>='      then exec_gte()
      when '&'       then exec_and()
      when '+'       then exec_or()
      when '!'       then exec_not()
      when 'include' then exec_include()
      else
        halt()
      end      
    end

    def exec_eql
      lhs, rhs = deref(*pop_pair())
      case
      when npe(lhs, rhs)
        push(Token.b(false))
      when type(lhs, rhs, :n, :s, :b)
        push(Token.b(lhs.value == rhs.value))
      when type(lhs, rhs, :v)
        push(Token.b(cmp(*pad(lhs.value, rhs.value), :eql)))
      else
        push(Token.b(false))
      end
    end

    def exec_lt
      lhs, rhs = deref(*pop_pair())
      case
      when npe(lhs, rhs)
        push(Token.b(false))
      when type(lhs, rhs, :n)
        push(Token.b(lhs.value < rhs.value))
      when type(lhs, rhs, :v)
        push(Token.b(cmp(*pad(lhs.value, rhs.value), :asc)))
      else
        push(Token.b(false))
      end
    end

    def exec_lte
      lhs, rhs = deref(*pop_pair())
      case
      when npe(lhs, rhs)
        push(Token.b(false))
      when type(lhs, rhs, :n)
        push(Token.b(lhs.value <= rhs.value))
      when type(lhs, rhs, :v)
        push(Token.b(cmp(*pad(lhs.value, rhs.value), :asc, :eql)))
      else
        push(Token.b(false))
      end
    end

    def exec_gt
      lhs, rhs = deref(*pop_pair())
      case
      when npe(lhs, rhs)
        push(Token.b(false))
      when type(lhs, rhs, :n)
        push(Token.b(lhs.value > rhs.value))
      when type(lhs, rhs, :v)
        push(Token.b(cmp(*pad(lhs.value, rhs.value), :dsc)))
      else
        push(Token.b(false))
      end
    end

    def exec_gte
      lhs, rhs = deref(*pop_pair())
      case
      when npe(lhs, rhs)
        push(Token.b(false))
      when type(lhs, rhs, :n)
        push(Token.b(lhs.value >= rhs.value))
      when type(lhs, rhs, :v)
        push(Token.b(cmp(*pad(lhs.value, rhs.value), :dsc, :eql)))
      else
        push(Token.b(false))
      end
    end

    def exec_and
      lhs, rhs = deref(*pop_pair())
      push(Token.b(type(lhs, rhs, :b) && lhs.value && rhs.value))
    end

    def exec_or
      lhs, rhs = deref(*pop_pair())
      push(Token.b(type(lhs, rhs, :b) && lhs.value || rhs.value))
    end

    def exec_not
      token = deref(@stack.pop).first
      push(Token.b(token.type == :b && ! token.value))
    end

    def exec_include
      lhs, rhs = deref(*pop_pair())
      case
      when type(lhs, rhs, :s)
        push(Token.b(lhs.value.include?(rhs.value)))
      else
        push(Token.b(false))
      end
    end

    def tokenize(line)
      line.scan(/"[^"]+"|\S+/).map do |word|
        case
        when word =~ /"(.*)"/            then Token.s($1)
        when word =~ /#(\w+)/            then Token.r($1.to_sym)
        when word =~ /'(\d+(?:\.\d+)*)'/ then Token.v($1)
        when word =~ /(-?\d+(?:\.\d+)?)/ then Token.n($1.to_f)
        when word =~ /(true|false)/      then Token.b($1 == 'true')
        else
          Token.o(word)
        end
      end
    end

    def pad(lhs, rhs)
      max = [lhs.count, rhs.count].max
      (max - lhs.count).times { lhs << 0 }
      (max - rhs.count).times { rhs << 0 }
      return lhs, rhs
    end

    def cmp(lhs, rhs, *values)
      values.include? ->() {
        lhs.zip(rhs).each do |lhs, rhs|
          return :asc if lhs < rhs
          return :dsc if lhs > rhs
        end
        return :eql
      }.call
    end

    def push(token)
      @stack << token
    end

    def pop_pair()
      rhs, lhs = @stack.pop, @stack.pop
      raise 'stack underflow' unless lhs && rhs
      return lhs, rhs
    end

    def type(lhs, rhs, *types)
      lhs.type == rhs.type && types.include?(lhs.type)
    end

    def deref(*tokens)
      tokens.map do |token|
        if token.type == :r
          @table[token.value] || :npe
        else
          token
        end
      end
    end

    def npe(*tokens)
      tokens.include?(:npe)
    end

  end

end