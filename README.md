# yodish

![yodish](logo/yodish.png)

## about

An expression language that uses postfix notation to apply operations to values on a stack. Used
to compare pre-defined values with values written in the expression, and returns either `true` or
`false` for the expression.

Predefined values are referred to with symbol references.

Words are separated by whitespace.

## example use case

Associating an item with an expression, and then evaluating the expression against some known values
to filter out that item from a list.

[example](yodish_example.rb)

## word forms

| example   | name                               |
| --------- | ---------------------------------- |
| `'1.2.3'` | version (a special form of number) |
| `-123457` | number                             |
| `"blimp"` | string                             |
| `true`    | boolean true                       |
| `false`   | boolean false                      |
| `#symbol` | symbol reference                   |

Any other words not matching the above patterns are treated as operations.

## operations

Operations are applied to values on the stack, popping values off and pushing
boolean values back on.

| operation | english equivalent      | operates on                      |
| --------- | ----------------------- | -------------------------------- |
| `=`       | equal                   | version, number, string, boolean |
| `<`       | less-than               | version, number                  |
| `>`       | greater-than            | version, number                  |
| `<=`      | less-than or equal      | version, number                  |
| `>=`      | greater-than or equal   | version, number                  |
| `&`       | and                     | boolean                          |
| `+`       | or                      | boolean                          |
| `!`       | not                     | boolean                          |
| `include` | string includes another | string                           |

## unknowns

An expression returns `false` if it contains an unknown operation.

An operation pushes `false` on to the stack when it uses a symbol reference that
is unknown, or is applied to a value of an unhandled type.

## examples

this C-style infix notation:

```
(name == "foo" && version >= '1.2.3') || (name == "bar" && version >= '4.5.6')
```

in postfix notation:

```
#name "foo" = #version '1.2.3' >= & #name "bar" = #version '4.5.6' >= & +
```

this C-style infix notation: _(where include is a function like `strstr`)_

```
include(items, "foo") && include(items, "bar")
```

in postfix notation:

```
#items "foo" include #items "bar" include &
```
