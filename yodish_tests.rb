#!/usr/bin/env ruby

require_relative 'yodish'

examples = <<~EOS
  X : xxx
  T : false false =
  F : false true =
  F : true false =
  T : true true =
  F : true "true" =
  F : true 1 =
  F : true '1' =
  T : 1 1 =
  T : 1 1.0 =
  T : -1 -1 =
  T : -1 -1.0 =
  F : 1 2 =
  F : 1 2.0 =
  F : 1 true =
  F : 1 "1" =
  F : 1 '1' =
  T : '1' '1' =
  T : '1' '1.0' =
  T : '1' '1.0.0' =
  T : '1.0' '1.0' =
  T : '1.0' '1.0.0' =
  T : "foo" "foo" =
  T : "foo bar" "foo bar" =
  F : "foo" "bar" =
  F : "1" '1' =
  F : "1" 1 =
  F : "true" true =
  F :  0  0           <
  F :  0  0           >
  F :  1  0           <
  T :  1  0           >
  T :  0  1           <
  F :  0  1           >
  T : -1  0           <
  F : -1  0           >
  F :  0  -1          <
  T :  0  -1          >
  T : '1'     '2'     <
  F : '1'     '2'     >
  T : '1'     '2.0'   <
  F : '1'     '2.0'   >
  T : '1'     '2.0.0' <
  F : '1'     '2.0.0' >
  T : '1.0'   '2'     <
  F : '1.0'   '2'     >
  T : '1.0'   '2.0'   <
  F : '1.0'   '2.0'   >
  T : '1.0'   '2.0.0' <
  F : '1.0'   '2.0.0' >
  T : '1.0.0' '2'     <
  F : '1.0.0' '2'     >
  T : '1.0.0' '2.0'   <
  F : '1.0.0' '2.0'   >
  T : '1.0.0' '2.0.0' <
  F : '1.0.0' '2.0.0' >
  T : '1'     '1.1'   <
  F : '1'     '1.1'   >
  T : '1'     '1.0.1' <
  F : '1'     '1.0.1' >
  T : '1'     '1.1.0' <
  F : '1'     '1.1.0' >
  T : '1.0'   '1.1'   <
  F : '1.0'   '1.1'   >
  T : '1.0'   '1.0.1' <
  F : '1.0'   '1.0.1' >
  T : '1.0'   '1.1.0' <
  F : '1.0'   '1.1.0' >
  T : '1.0.0' '1.1'   <
  F : '1.0.0' '1.1'   >
  T : '1.0.0' '1.0.1' <
  F : '1.0.0' '1.0.1' >
  T : '1.0.0' '1.1.0' <
  F : '1.0.0' '1.1.0' >
  F : '2'     '1'     <
  T : '2'     '1'     >
  F : '2.0'   '1'     <
  T : '2.0'   '1'     >
  F : '2.0.0' '1'     <
  T : '2.0.0' '1'     >
  F : '2'     '1.0'   <
  T : '2'     '1.0'   >
  F : '2.0'   '1.0'   <
  T : '2.0'   '1.0'   >
  F : '2.0.0' '1.0'   <
  T : '2.0.0' '1.0'   >
  F : '2'     '1.0.0' <
  T : '2'     '1.0.0' >
  F : '2.0'   '1.0.0' <
  T : '2.0'   '1.0.0' >
  F : '2.0.0' '1.0.0' <
  T : '2.0.0' '1.0.0' >
  F : '1.1'   '1'     <
  T : '1.1'   '1'     >
  F : '1.0.1' '1'     <
  T : '1.0.1' '1'     >
  F : '1.1.0' '1'     <
  T : '1.1.0' '1'     >
  F : '1.1'   '1.0'   <
  T : '1.1'   '1.0'   >
  F : '1.0.1' '1.0'   <
  T : '1.0.1' '1.0'   >
  F : '1.1.0' '1.0'   <
  T : '1.1.0' '1.0'   >
  F : '1.1'   '1.0.0' <
  T : '1.1'   '1.0.0' >
  F : '1.0.1' '1.0.0' <
  T : '1.0.1' '1.0.0' >
  F : '1.1.0' '1.0.0' <
  T : '1.1.0' '1.0.0' >
  T : 0 0 <=
  T : 0 0 >=
  T : 0 1 <=
  F : 0 1 >=
  F : 1 0 <=
  T : 1 0 >=
  T : '1' '1' <=
  T : '1' '1' >=
  T : '1' '1.1' <=
  F : '1' '1.1' >=
  F : '1.1' '1' <=
  T : '1.1' '1' >=
  F : false false &
  F : false true &
  F : true false &
  T : true true &
  F : false false +
  T : false true +
  T : true false +
  T : true true +
  F : true !
  T : false !
  F : 0 !
  F : 1 !
  F : "hello" !
  F : '0' !
  F : '1' !
  T : "aa" "a" include
  F : "aa" "b" include
  T : "ab" "b" include
  F : "a" "aa" include
  F : "b" "aa" include
  F : "b" "ab" include
  F : "1" 1 include
  F : "1" '1' include
  F : "1" true include
  F : 1 "1" include
  F : '1' "1" include
  F : true "1" include
  F : #xxx true =
  F : #xxx false =
  T : #b_true true =
  F : #b_false true =
  F : true #xxx =
  F : false #xxx =
  T : true #b_true =
  F : true #b_false =
  F : #xxx 0 =
  F : #xxx 0 <
  F : #xxx 0 <=
  F : #xxx 0 >=
  F : 0 #xxx =
  F : 0 #xxx <
  F : 0 #xxx <=
  F : 0 #xxx >=
  T : #n_0 0 =
  F : #n_0 0 <
  T : #n_0 0 <=
  T : #n_0 0 >=
  F : #n_0 1 =
  T : #n_0 1 <
  T : #n_0 1 <=
  F : #n_0 1 >=
  T : #n_0 #n_0 =
  F : #n_0 #n_0 <
  T : #n_0 #n_0 <=
  T : #n_0 #n_0 >=
  F : #n_0 #n_1 =
  T : #n_0 #n_1 <
  T : #n_0 #n_1 <=
  F : #n_0 #n_1 >=
  F : #n_1 #n_0 =
  F : #n_1 #n_0 <
  F : #n_1 #n_0 <=
  T : #n_1 #n_0 >=
  T : #n_1 #n_1 =
  F : #n_1 #n_1 <
  T : #n_1 #n_1 <=
  T : #n_1 #n_1 >=
  T : #v_1 '1' =
  F : #v_1 '1' <
  T : #v_1 '1' <=
  T : #v_1 '1' >=
  F : #v_1 '2' =
  T : #v_1 '2' <
  T : #v_1 '2' <=
  F : #v_1 '2' >=
  T : #v_1 #v_1 =
  F : #v_1 #v_1 <
  T : #v_1 #v_1 <=
  T : #v_1 #v_1 >=
  F : #v_1 #v_2 =
  T : #v_1 #v_2 <
  T : #v_1 #v_2 <=
  F : #v_1 #v_2 >=
  F : #v_2 #v_1 =
  F : #v_2 #v_1 <
  F : #v_2 #v_1 <=
  T : #v_2 #v_1 >=
  T : #v_2 #v_2 =
  F : #v_2 #v_2 <
  T : #v_2 #v_2 <=
  T : #v_2 #v_2 >=
  F : #b_true  false    &
  T : #b_true  true     &
  F : #b_false false    &
  F : #b_false true     &
  F : false    #b_false &
  F : false    #b_true  &
  F : true     #b_false &
  T : true     #b_true  &
  F : #b_false #b_false &
  F : #b_false #b_true  &
  F : #b_true  #b_false &
  T : #b_true  #b_true  &
  T : #b_true  false    +
  T : #b_true  true     +
  F : #b_false false    +
  T : #b_false true     +
  F : false    #b_false +
  T : false    #b_true  +
  T : true     #b_false +
  T : true     #b_true  +
  F : #b_false #b_false +
  T : #b_false #b_true  +
  T : #b_true  #b_false +
  T : #b_true  #b_true  +
  T : #b_false !
  F : #b_true  !
  T : #s_aa "a" include
  F : #s_aa "b" include
  T : #s_ab "b" include
  F : #s_a "aa" include
  F : #s_b "aa" include
  F : #s_b "ab" include
  T : "aa" #s_a include
  F : "aa" #s_b include
  T : "ab" #s_b include
  F : "a" #s_aa include
  F : "b" #s_aa include
  F : "b" #s_ab include
  T : #s_aa #s_a include
  F : #s_aa #s_b include
  T : #s_ab #s_b include
  F : #s_a #s_aa include
  F : #s_b #s_aa include
  F : #s_b #s_ab include
EOS

yodish = Yodish::Evaluator.new do
  {
    :b_true  => Yodish::Token.b(true),
    :b_false => Yodish::Token.b(false),
    :n_0     => Yodish::Token.n(0),
    :n_1     => Yodish::Token.n(1),
    :s_a     => Yodish::Token.s("a"),
    :s_b     => Yodish::Token.s("b"),
    :s_aa    => Yodish::Token.s("aa"),
    :s_ab    => Yodish::Token.s("ab"),
    :v_1     => Yodish::Token.v("1"),
    :v_2     => Yodish::Token.v("2"),
  }
end

examples.each_line do |line|
  expect, exp = line.split(':').map(&:strip)
  failed      = "fail \u2718 => #{line}"

  begin
    result = yodish.eval exp
  rescue => e
    STDERR.puts "error \u2718 => #{line} (#{e.message})"
    raise
  end

  case expect
  when 'T' 
    raise failed unless result
    raise failed if     yodish.halts
  when 'F'
    raise failed if     result
    raise failed if     yodish.halts
  when 'X'
    raise failed if     result
    raise failed unless yodish.halts
  end
end

puts "pass \u2714"
