#!/usr/bin/env ruby

require_relative 'yodish'

@yodish = Yodish::Evaluator.new do
  {
    :platform     => Yodish::Token.s("apple"),
    :capabilities => Yodish::Token.s("microphone camera webgl"),
    :version      => Yodish::Token.v("2.0.10"),
    :brand        => Yodish::Token.s("banana-man"),
    :signup       => Yodish::Token.b(true),
    :month        => Yodish::Token.n(10),
  }
end

def q?(message, expression)
  puts "#{message} " + (@yodish.eval(expression) ? "\u2714" : "\u2718")
end

q? 'apple'                               , '#platform "apple" ='
q? 'apple, version >= 2.0.9'             , '#platform "apple" = #version \'2.0.9\' >= &'
q? 'apple, version >= 2.0.9, camera'     , '#platform "apple" = #version \'2.0.9\' >= & #capabilities "camera" include &'
q? 'camera and microphone'               , '#capabilities "camera" include #capabilities "microphone" include &'
q? 'android'                             , '#platform "android" ='
q? 'apple, version <= 2.0.9, camera'     , '#platform "apple" = #version \'2.0.9\' <= & #capabilities "camera" include &'
q? 'not accelerometer'                   , '#capabilities "accelerometer" include !'
q? 'banana-man or super-ted'             , '"banana-man super-ted" #brand include'
q? 'banana-man or super-ted (alternate)' , '"banana-man" #brand = "super-ted" #brand = +'
q? 'signed in'                           , '#signup true &'
q? 'winter month'                        , '#month 10 >= #month 2 <= +'
q? 'unknown symbol'                      , '#unknown "foo" ='
